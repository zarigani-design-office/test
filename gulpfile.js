/*===初期設定====*/
var folders = {
	scss : 'sass',//scssフォルダ
	css : './',//出力するcssフォルダ
	js : 'js',//jsフォルダ
	image_src : 'src',//圧縮前の画像フォルダ
	image_dist : 'img',//圧縮後の画像フォルダ
	sync: true,//BrowserSyncを使用するか。真偽値
	proxy : 'localhost/architecture/',//BrowserSyncのプロキシ使わない場合は空。PHPなど動かす時。localhost:8888/project/など
};
/*===初期設定ここまで====*/


/*+++gulpプラグインの読み込み+++*/
var gulp = require('gulp');
// エラーで止めない
var plumber = require('gulp-plumber');
// Sassをコンパイルするプラグインの読み込み
var sass = require('gulp-sass');
// 変更されたファイルだけを処理させるプラグインの読み込み
var changed  = require('gulp-changed');
// 画像圧縮プラグインの読み込み
var imagemin = require('gulp-imagemin');
var imageminJpg = require('imagemin-jpeg-recompress');
var imageminPng = require('imagemin-pngquant');
var imageminGif = require('imagemin-gifsicle');
var svgmin = require('gulp-svgmin');
//BrowserSync
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer')
/*+++gulpプラグインの読み込みここまで+++*/


/*+++SCSSコンパイル+++*/
gulp.task('scss',function(){
	gulp.src(folders.scss + '/*' + '.scss')
	.pipe(plumber())
	.pipe(sass({outputStyle : 'expanded'}))
	.pipe(autoprefixer({
		browsers: ['last 2 version'],
		grid: true
	}))
	.pipe(gulp.dest(folders.css));
})



/*+++画像圧縮+++*/
gulp.task('imagemin', function(){
	var srcGlob = folders.image_src + '/**/*.+(jpg|jpeg|png|gif)';
	var dstGlob = folders.image_dist;
	gulp.src( srcGlob )
		.pipe(plumber())
		.pipe(changed( dstGlob ))
		.pipe(imagemin([
		imageminPng(),
		imageminJpg(),
		imageminGif({
			interlaced: false,
			optimizationLevel: 3,
			colors:180
		})
	]))
		.pipe(gulp.dest( dstGlob ));
});

// svg画像の圧縮タスク
gulp.task('svgmin', function(){
	var srcGlob = folders.image_src + '/**/*.+(svg)';
	var dstGlob = folders.image_dist;
	gulp.src( srcGlob )
		.pipe(plumber())
		.pipe(changed( dstGlob ))
		.pipe(svgmin())
		.pipe(gulp.dest( dstGlob ));
});
/*+++画像圧縮ここまで+++*/



/*+++BrowserSync+++*/
//プロキシの設定の有無
if(folders.proxy == ''){
	var browserSync_int_setting = {
		server: {
			baseDir: './'
		}
	}
}else{
	var browserSync_int_setting = {
		proxy: folders.proxy,
	}
}

if(folders.sync){
	gulp.task('browser-sync', function() {
		browserSync.init(browserSync_int_setting);
	});
	//ブラウザリロード
	gulp.task('bs-reload', function () {
		browserSync.reload();
	});
}else{
	gulp.task('browser-sync', function () {
		return;
	})
}


/*+++BrowserSyncここまで+++*/



/*+++監視+++*/
gulp.task('watch',function(){
	gulp.watch([folders.scss + '/*.scss'],['scss']);
	gulp.watch([folders.scss + '/**/*.scss'],['scss']);
	gulp.watch([folders.image_src + '/**/*'], ['imagemin','svgmin']);
	if(folders.sync){
		gulp.watch('./*', ['bs-reload']);
		gulp.watch(folders.scss + '/*', ['bs-reload']);
		gulp.watch(folders.js + '/*', ['bs-reload']);
		gulp.watch(folders.image_src + '/*', ['bs-reload']);
	}
})
gulp.task('default', ['watch','browser-sync'],function(){
	
});